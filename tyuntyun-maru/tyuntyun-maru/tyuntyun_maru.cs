﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreTweet;
using CoreTweet.Core;
using CoreTweet.Rest;
using CoreTweet.Streaming;
using System.Net;
using System.IO;
using System.Xml.Linq;
using Sgml;

namespace tyuntyun_maru.Core
{
    public class TwitterUtil
    {
        // Consumer Key (API Key)
        private const string API_KEY = "v7zDdKdmCjWjPXGW5EMAW13Dz";
        //Consumer Secret (API Secret)
        private const string API_SECRET = "vCqabdxvK6jAZYGxGDqlhe9bnGiwrxOd1ug6Sxq0MnHzrxWVEm";

        private const string ACCESS_TOKEN = "184876485-gyJ0esPNrwhuTEp4b3m9bHUznqey2Gc53Wp23pk9";
        private const string ACCESS_TOKEN_SECRET = "rNqvGEQ0aQY5PeuMdNMqpo2IL2cD6Z5ZohNj1h29oPNYS";

        private TokensBase token;
//        private string bearerToken;

        private Dictionary<string, OAuth.OAuthSession> oAuthSessions = new Dictionary<string, OAuth.OAuthSession>();

        // OAuth認証でトークンを取得する
        //public void initOAuth()
        //{
        //    var session = OAuth.Authorize(API_KEY, API_SECRET);

        //    string pinCode = "";

        //    WebClient webClient = new WebClient();
        //    using (var stream = webClient.OpenRead(session.AuthorizeUri.OriginalString))
        //    using (var sr = new StreamReader(stream))
        //    {
        //        XDocument xml = ParseHtml(sr);
        //        xml.Save("twitter.xml");

        //        var ns = xml.Root.Name.Namespace;
        //        foreach (var elem in xml.Descendants(ns + "KBD").Take(1))
        //        {
        //            pinCode = elem.Value;
        //        }
        //    }

        //    token = OAuth.GetTokens(session, pinCode);
        //}

        /// <summary>
        /// すでに発行されたトークンを取得する
        /// </summary>
        public void initCreateToken()
        {
            token = CoreTweet.Tokens.Create(API_KEY, API_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
        }

        public XDocument ParseHtml(TextReader reader)
        {
            using (var sgmlReader = new SgmlReader() { DocType = "HTML", CaseFolding = CaseFolding.ToUpper })
            {
                sgmlReader.InputStream = reader;
                return XDocument.Load(sgmlReader);
            }
        }

        /// <summary>
        /// 検索したツイートを取得する
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="num"></param>
        public void Search(string keyword, int num = 50)
        {
            var result = token.Search.Tweets(count => num, q => keyword);

            foreach (var tweet in result)
            {
                Console.WriteLine("{0}: {1}",
                    tweet.User.ScreenName, tweet.Text);
                Console.WriteLine("---------");
            }
        }

        /// <summary>
        /// お気に入りリストの画像付きツイートを取得する
        /// </summary>
        /// <param name="num"></param>
        public List<Status> FavoritesWithMedia(int num = 50)
        {
            var favs = token.Favorites.List(count => num);

            var mediaTweets = favs
                .Where(tweet => tweet.Entities.Media != null)
                .Where(tweet => 
                    tweet.Entities.Media.First().MediaUrl.EndsWith(".jpg") ||
                    tweet.Entities.Media.First().MediaUrl.EndsWith(".png") ||
                    tweet.Entities.Media.First().MediaUrl.EndsWith(".gif"))
                .OrderByDescending(tweet => tweet.CreatedAt);

            return mediaTweets.ToList();
        }
    }
}

