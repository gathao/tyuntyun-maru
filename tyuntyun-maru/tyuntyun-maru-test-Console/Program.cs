﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using tyuntyun_maru.Core;
using CoreTweet;
using System.Diagnostics;
using System.Net;
using System.IO;
using Gathao.Models.Extensions;

namespace tyuntyun_maru_test_Console
{
    class Program
    {
        private const string SLASH = "/";
        private const string OUTPUT_DIR = "TwitterFavoritImages";

        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            WebClient webClient = new WebClient();

            TwitterUtil twitter = new TwitterUtil();
            twitter.initCreateToken();

            var favoritesList = twitter.FavoritesWithMedia(200);

            foreach (var tweet in favoritesList)
            {
                //string userDir = sb.Append(OUTPUT_DIR, SLASH, tweet.User.Name);
                //Directory.CreateDirectory(userDir);
                Directory.CreateDirectory(OUTPUT_DIR);

                foreach (var media in tweet.Entities.Media)
                {
                    string url = media.MediaUrl;

                    sb.Append(OUTPUT_DIR, SLASH, Path.GetFileName(url));

                    webClient.DownloadFile(url, sb.ToString());

                    sb.Clear();

                    Thread.Sleep(500);
                }
            }
        }
    }
}
