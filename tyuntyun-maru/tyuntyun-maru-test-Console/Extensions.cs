﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Gathao.Models.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// StringBuilderに文字列のparamsを使って連結するメソッドを追加
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Append(this StringBuilder sb, params string[] args)
        {
            foreach(string str in args)
            {
                sb.Append(str);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 任意の型・任意の数字の範囲制限
        /// [使い方] int a = 4.(2, 3);    double a = b.(0, 255);　とか
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0)
            {
                return min;
            }
            else if (val.CompareTo(max) > 0)
            {
                return max;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 値の最大値
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static T Max<T>(this T val, T max) where T : IComparable<T>
        {
            if (val.CompareTo(max) > 0)
            {
                return max;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 値の最小値
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static T Min<T>(this T val, T min) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0)
            {
                return min;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 現在の文字列と、指定した文字列を比較します。大文字と小文字は区別されません。
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static bool Compare(this string str1, string str2)
        {
            return string.Equals(str1, str2, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}
